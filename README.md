# Maze Solver

`mazeSolver.mips` contains about 500 lines of MIPS assembly, intended to be
run through the m4 preprocessor before being assembled. Once assembled, it can
direct a robot to solve a maze. The robot should report information through
register `$t9` as follows:

- Bit 31 to Bit 24: The current row position, in 8-bit two’s complement.
- Bit 23 to Bit 16: The current column position, in 8-bit two’s complement.
- Bit 11: 1 iff facing north (up).
- Bit 10: 1 iff facing east (right).
- Bit 9: 1 iff facing south (down).
- Bit 8: 1 iff facing west (left).
- Bit 3: 1 iff there is a wall in front of the car robot.
- Bit 2: 1 iff there is a wall on the left side of the car robot.
- Bit 1: 1 iff there is a wall on the right side of the car robot.
- Bit 0: 1 iff there is a wall behind the car robot.

The robot should also poll register `$t8` and respond to its value as follows:

- Move forward one block if the value is 1.
- Turn left 90 degrees if the value is 2.
- Turn right 90 degrees if the value is 3.
- Update the status without moving if the value is 4.
