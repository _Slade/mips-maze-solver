size8: mazeSolver.mips
	m4 -DSIZE=8 $< >| mazeSolver.asm

size4: mazeSolver.mips
	m4 -DSIZE=4 $< >| mazeSolver.asm

size2: mazeSolver.mips
	m4 -DSIZE=2 $< >| mazeSolver.asm

test : test.c
	gcc -Wall -std=c99 -g -o $@ $<

